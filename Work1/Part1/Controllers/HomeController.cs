﻿using Part1.football;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Part1.country;
using System.Xml.Linq;
using Part1.Models;

namespace Part1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Info football = new Info();
            List < tGameInfo > footballGames = football.AllGames().ToList();
            country.country Country = new country.country();
            var param = Country.GetCountries();
            var countries = XDocument.Parse(param).Descendants("Name").Select(arg => arg.Value).ToList();

            var model = new GeneralModel
            {
                GameInfoList = footballGames,
                CountriesList = countries
            };
            return View(model);
        }

        public ActionResult About()
        {
            Info football = new Info();
            var param1 = football.Teams();
            var teamInfoList = param1.Select(i =>
            {
                return new TeamModel
                {
                    TeamInfoList = i.sName
                };
            }).ToList();

            return View(teamInfoList);
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
﻿using Part1.football;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Part1.Models
{
    public class GeneralModel
    {
        public List<tGameInfo> GameInfoList { get; set; }
        public List<string> CountriesList { get; set; }
    }
}
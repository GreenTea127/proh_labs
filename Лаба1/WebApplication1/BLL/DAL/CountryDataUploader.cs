﻿using BLL.CountryRef;
using System.Threading.Tasks;

namespace BLL.DAL
{
    class CountryDataUploader
    {
        country _countryFetch;

        public CountryDataUploader()
        {
            _countryFetch = new country();
        }

        public Task<string> GetCountryDataXmlAsync()
        {

            return Task.Run(() =>
            {
                return GetCountryDataXml();
            });
        }

        public string GetCountryDataXml()
        {
            return _countryFetch.GetCountries();
        }
    }
}

﻿using BLL.FootballRef;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.DAL
{
    class FootballGamesDataUploader
    {
        Info _footWS;

        public FootballGamesDataUploader()
        {
            _footWS = new Info();
        }

        public IReadOnlyList<tGameInfo> GetAllGames()
        {
            IReadOnlyList<tGameInfo> footballGames = _footWS.AllGames().ToList();
            return footballGames;
        }

        public Task<IReadOnlyList<tGameInfo>> GetAllGamesAsync()
        {
            return Task.Run(() =>
            {
                return GetAllGames();
            });
        }

        public IReadOnlyList<tTeamInfo> GetTeamsInfo()
        {
            IReadOnlyList<tTeamInfo> teamsInfo = _footWS.Teams().ToList();
            return teamsInfo;
        }

        public Task<IReadOnlyList<tTeamInfo>> GetTeamInfoAsync()
        {
            return Task.Run(() =>
            {
                return GetTeamsInfo();
            });
        }
    }
}

﻿using BLL.WeatherRef;
using System.Threading.Tasks;

namespace BLL.DAL
{
    class WeatherDataUploader
    {
        GlobalWeather _weatherFetch;

        public WeatherDataUploader()
        {
            _weatherFetch = new GlobalWeather();
        }

        #region Get Weather Data Xml By Country
        public Task<string> GetWeatherDataXmlByCountryAsync(string countryName)
        {

            return Task.Run(() =>
            {
                return GetWeatherDataXmlByCountry(countryName);
            });
        }

        public string GetWeatherDataXmlByCountry(string countryName)
        {
            return _weatherFetch.GetCitiesByCountry(countryName);
        }
        #endregion

        #region Get Weather Data By Country And City
        public Task<string> GetWeatherDataByCountryAndCityAsync(string countryName, string cityName)
        {

            return Task.Run(() =>
            {
                return GetWeatherDataByCountryAndCity(countryName, cityName);
            });
        }

        public string GetWeatherDataByCountryAndCity(string countryName, string cityName)
        {
            return _weatherFetch.GetWeather(countryName, cityName);
        }
        #endregion
    }
}

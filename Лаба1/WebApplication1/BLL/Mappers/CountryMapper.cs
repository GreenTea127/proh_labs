﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace BLL.Mappers
{
    class XmlMapper
    {
        public IReadOnlyList<string> ConvertCountryXmlToListByAttributeName(string xml, string attributeName)
        {
            return XDocument.Parse(xml)
                .Descendants(attributeName)
                .Select(arg => arg.Value)
                .ToList();
        }
    }
}

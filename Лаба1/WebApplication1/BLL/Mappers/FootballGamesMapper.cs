﻿using BLL.FootballRef;
using Models;
using System.Collections.Generic;
using System.Linq;

namespace BLL.Mappers
{
    class FootballGamesMapper
    {
        public GameInfoModel MapSingleGameInfoToModel(tGameInfo gameInfo)
        {
            return new GameInfoModel
            {
                StadiumName = gameInfo.StadiumInfo.sStadiumName,
                StadiumWikiUrl = gameInfo.StadiumInfo.sWikipediaURL,
                Team1Name = gameInfo.Team1.sName,
                Team2Name = gameInfo.Team2.sName,
                Team1WikiUrl = gameInfo.Team1.sWikipediaURL,
                Team2WikiUrl = gameInfo.Team2.sWikipediaURL
            };
        }

        public IReadOnlyList<GameInfoModel> MapGamesInfoToModels(IReadOnlyList<tGameInfo> tgamesInfo)
        {
            return tgamesInfo
                .Select(MapSingleGameInfoToModel)
                .ToList();
        }

        public TeamInfoModel MapSingleTeamInfoToModel(tTeamInfo teamInfo)
        {
            return new TeamInfoModel
            {
                Flag = teamInfo.sCountryFlag,
                Name = teamInfo.sName,
                WikiUrl = teamInfo.sWikipediaURL
            };
        }

        public IReadOnlyList<TeamInfoModel> MapTeamsInfoToModels(IReadOnlyList<tTeamInfo> tteamsInfo)
        {
            return tteamsInfo
                .Select(MapSingleTeamInfoToModel)
                .ToList();
        }
    }
}

﻿using BLL.DAL;
using BLL.Mappers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class CountryService
    {
        private CountryDataUploader _countryUploader;
        private XmlMapper _mapper;

        public CountryService()
        {
            _countryUploader = new CountryDataUploader();
            _mapper = new XmlMapper();
        }

        public async Task<IReadOnlyList<string>> GetAllCountriesList()
        {
            string xmlData = await _countryUploader.GetCountryDataXmlAsync();
            IReadOnlyList<string> countryList = _mapper.ConvertCountryXmlToListByAttributeName(xmlData, "Name");

            return countryList;
        }
    }
}

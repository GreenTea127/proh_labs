﻿using BLL.DAL;
using BLL.FootballRef;
using BLL.Mappers;
using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class FootballGamesService
    {
        private FootballGamesDataUploader _uploader;
        private FootballGamesMapper _mapper;

        public FootballGamesService()
        {
            _uploader = new FootballGamesDataUploader();
            _mapper = new FootballGamesMapper();
        }

        public async Task<IReadOnlyList<GameInfoModel>> GetGamesInfoListAsync()
        {
            IReadOnlyList<tGameInfo> tGameInfoList = await _uploader.GetAllGamesAsync();
            IReadOnlyList<GameInfoModel> gamesInfoList = _mapper.MapGamesInfoToModels(tGameInfoList);

            return gamesInfoList;
        }

        public async Task<IReadOnlyList<TeamInfoModel>> GetTeamsAsync()
        {
            IReadOnlyList<tTeamInfo> teamInfoList = await _uploader.GetTeamInfoAsync();
            IReadOnlyList<TeamInfoModel> teamsModelList = _mapper.MapTeamsInfoToModels(teamInfoList);

            return teamsModelList;
        }
    }
}

﻿using BLL.DAL;
using BLL.Mappers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class WeatherService
    {
        private WeatherDataUploader _uploader;
        private XmlMapper _mapper;

        public WeatherService()
        {
            _uploader = new WeatherDataUploader();
            _mapper = new XmlMapper();
        }

        public async Task<IReadOnlyList<string>> GetGamesInfoList(string countryName)
        {
            string weatherXml = await _uploader.GetWeatherDataXmlByCountryAsync(countryName);
            IReadOnlyList<string> sityList = _mapper.ConvertCountryXmlToListByAttributeName(weatherXml,"City");

            return sityList;
        }

        public async Task<string> GetWeatherByCountryAndCity(string countryName, string cityName)
        {
            string result = await _uploader.GetWeatherDataByCountryAndCityAsync(countryName, cityName);
            return result;
        }
    }
}

﻿namespace Models
{
    public class GameInfoModel
    {
        public string Team1Name { get; set; }
        public string Team2Name { get; set; }
        public string Team1WikiUrl { get; set; }
        public string Team2WikiUrl { get; set; }
        public string StadiumWikiUrl { get; set; }
        public string StadiumName { get; set; }
    }
}

﻿namespace Models
{
    public class TeamInfoModel
    {
        public string Name { get; set; }
        public string WikiUrl { get; set; }
        public string Flag { get; set; }
    }
}

﻿using System.Web.Mvc;
using System.Web.Routing;

namespace WebApplication1
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Weather",
                url: "{controller}/{action}/{countryName}",
                defaults: new { controller = "Home", action = "About" }
            );

            routes.MapRoute(
                name: "WeatherCity",
                url: "{controller}/{action}/{countryName}/{cityName}",
                defaults: new { controller = "Home", action = "About" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}",
                defaults: new { controller = "Home", action = "Task1"}
            );

            
        }
    }
}

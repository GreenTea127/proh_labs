﻿using BLL.Services;
using Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private CountryService _countryService = new CountryService();
        private FootballGamesService _footballGamesService = new FootballGamesService();
        private WeatherService _weatherService = new WeatherService();

        public async Task<ActionResult> Task1()
        {
            IReadOnlyList<string> countries = await _countryService.GetAllCountriesList();
            IReadOnlyList<GameInfoModel> gameInfoList = await _footballGamesService.GetGamesInfoListAsync();

            var model = new CountriesAndGamesModel
            {
                CountriesList = countries,
                GameInfoList = gameInfoList
            };

            return View(model);
        }

        public async Task<ActionResult> Task2()
        {
            IReadOnlyList<TeamInfoModel> gameInfoList = await _footballGamesService.GetTeamsAsync();

            return View(gameInfoList);
        }

        public async Task<ActionResult> Task5()
        {
            IReadOnlyList<TeamInfoModel> gameInfoList = await _footballGamesService.GetTeamsAsync();

            return View(gameInfoList);
        }

        public async Task<ActionResult> About()
        {
            IReadOnlyList<string> countries = await _countryService.GetAllCountriesList();

            return View(countries);
        }

        public async Task<ActionResult> AboutCountry(string countryName)
        {
            IReadOnlyList<string> citiesList = await _weatherService.GetGamesInfoList(countryName);
            var countyAndCities = new CountryAndCitiesModel
            {
                CountryName = countryName,
                CitiesList = citiesList
            };

            return View(countyAndCities);
        }

        public async Task<ActionResult> AboutCountryAndSity(string countryName, string cityName)
        {
            Convert celsToFarhConverter = new Convert();
            string result = await _weatherService.GetWeatherByCountryAndCity(countryName, cityName);

            var weatherModel = new CityWeatherModel
            {
                City = cityName,
                Country = countryName,
                WeatherDescription = result
            };

            if(result == "Data Not Found")
            {
                weatherModel.Cels = 20d;
                weatherModel.Farh = celsToFarhConverter.CelsiusToFahrenheit(weatherModel.Cels);
            }

            return View(weatherModel);
        }


        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
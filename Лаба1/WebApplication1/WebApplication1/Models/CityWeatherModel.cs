﻿namespace WebApplication1.Models
{
    public class CityWeatherModel
    {
        public string Country { get; set; }
        public string City { get; set; }
        public string WeatherDescription { get; set; }
        public double Cels { get; set; }
        public double Farh { get; set; }
    }
}
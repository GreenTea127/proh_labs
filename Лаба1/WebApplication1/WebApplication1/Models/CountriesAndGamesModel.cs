﻿using Models;
using System.Collections.Generic;

namespace WebApplication1.Models
{
    public class CountriesAndGamesModel
    {
        public IReadOnlyList<GameInfoModel> GameInfoList { get; set; }
        public IReadOnlyList<string> CountriesList { get; set; }
    }
}
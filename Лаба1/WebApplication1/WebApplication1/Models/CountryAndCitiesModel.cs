﻿using System.Collections.Generic;

namespace WebApplication1.Models
{
    public class CountryAndCitiesModel
    {
        public string CountryName { get; set; }
        public IReadOnlyList<string> CitiesList { get; set; }
    }
}
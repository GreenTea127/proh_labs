﻿; $(function () {
    const CHECKBOX_SELECTOR = '.country-filter-checkbox';
    const DROPDOWN_SELECTOR = '.select-form';
    const SELECT_SELECTOR = '#games-select';

    const gridRowHiddenFieldClassName = 'filtered';

    $(CHECKBOX_SELECTOR).change(function () {
        $(SELECT_SELECTOR).val('default');
        if (this.checked) {
            $(DROPDOWN_SELECTOR).show('fast');
        } else {           
            $(DROPDOWN_SELECTOR).hide('fast');
            $(`.dataRow.${gridRowHiddenFieldClassName}`).removeClass(gridRowHiddenFieldClassName);
        }
    });

    $(SELECT_SELECTOR).change(function(){
        const countryName = this.value;
        
        $(`.dataRow.${gridRowHiddenFieldClassName}`).removeClass(gridRowHiddenFieldClassName);

        if(countryName == 'default'){
            return;
        }

        let $gridItems =  $(`.game-info-table .dataRow`);

        $gridItems.each(function(index,item){
            const $item = $(item); 
            const team1name = $item.find('td.team1').text();
            const team2name = $item.find('td.team2').text();

            if(team1name !== countryName && team2name !== countryName){
                $item.addClass(gridRowHiddenFieldClassName);
            }
        });
    });
});